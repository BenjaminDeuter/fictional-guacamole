package sec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AdministrationController {
    
    @Autowired
    private GruntService gruntService;
    
    @Autowired
    private ThoughtService thoughtService;
    
    @RequestMapping(value="/admin", method = RequestMethod.GET)
    public String getOverview(Model model) {
        model.addAttribute("grunts", gruntService.recvGrunts());
        model.addAttribute("thoughts", thoughtService.recvThoughts());
        
        return "/admin/administration";
    }
    
    @RequestMapping(value="/admin/destroy/grunt/{id}", method = RequestMethod.POST)
    public String destroyGrunt(@PathVariable Integer id) {
        gruntService.destroyGrunt(id);
        return "redirect:/admin";
    }
    
    @RequestMapping(value="/admin/delete/thought/{id}", method = RequestMethod.POST)
    public String deleteThought(@PathVariable Integer id) {
        thoughtService.deleteThought(id);
        return "redirect:/admin";
    }
    
}
