package sec;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {
    
    @RequestMapping("/coursepage")
    public String external() {
        return "/external/coursepage";
    }
    
    @RequestMapping("*")
    public String returnMain() {
        return "redirect:/gruntsHub";
    }
    
}
