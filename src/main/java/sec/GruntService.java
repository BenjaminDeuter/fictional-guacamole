package sec;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.h2.tools.RunScript;
import org.springframework.stereotype.Service;

@Service
public class GruntService {
    
    private String db = "jdbc:h2:file:./database";
    
    public GruntService(){
        try {
            Connection c = DriverManager.getConnection(db, "sa", "");
            RunScript.execute(c, new FileReader("src/main/resources/sql/grunts.sql"));
            c.close();
        } catch (FileNotFoundException | SQLException ex) {
            Logger.getLogger(GruntService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<Grunt> recvGrunts() {
        List<Grunt> result = new ArrayList<>();
        try {
            Connection c = DriverManager.getConnection(db, "sa", "");
            ResultSet rs = c.createStatement().executeQuery("SELECT id, message FROM Grunts");
            while (rs.next()) {
                result.add(new Grunt(rs.getInt("id"), rs.getString("message")));
            }
            rs.close();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(GruntService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public void newGrunt(Grunt g) {
        try {
            Connection c = DriverManager.getConnection(db, "sa", "");
            String db_query = "INSERT INTO Grunts (id, message) VALUES ('" + g.getId().toString() + "', '" + g.getMessage() + "');";
            Statement s = c.createStatement();
            s.execute(db_query);
            s.close();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(GruntService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void destroyGrunt(Integer id) {
        try {
            Connection c = DriverManager.getConnection(db, "sa", "");
            String db_query = "DELETE FROM Grunts WHERE id = "+Integer.toString(id)+";";
            Statement s = c.createStatement();
            s.execute(db_query);
            s.close();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(GruntService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
