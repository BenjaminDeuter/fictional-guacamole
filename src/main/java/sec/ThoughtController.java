package sec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ThoughtController {
    
    @Autowired
    private ThoughtService thoughtService;
    
    @RequestMapping(value="/myThoughts", method = RequestMethod.GET)
    public String recvThoughts(Model model) {
        model.addAttribute("thoughts", thoughtService.recvThoughts());
        return "myThoughts";
    }
    
    @RequestMapping(value="/myThoughts", method = RequestMethod.POST)
    public String newThought(@RequestParam String text) {
        thoughtService.addThought(new Thought(text));
        return "redirect:/myThoughts";
    }
    
}
