package sec;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder a) throws Exception {
        
        a.inMemoryAuthentication()
         .withUser("user")
         .password("user")
         .roles("USER");
        
        a.inMemoryAuthentication()
         .withUser("admin")
         .password("admin")
         .roles("ADMIN");
        
    }
    
    @Override
    public void configure(HttpSecurity h) {
        try {
            
            h.formLogin();
            
            h.authorizeRequests()
             .anyRequest()
             .authenticated();
            
            h.logout()
             .permitAll();
            
            h.csrf()
             .disable();
            
        } catch (Exception ex) {
            Logger.getLogger(SecurityConfiguration.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
