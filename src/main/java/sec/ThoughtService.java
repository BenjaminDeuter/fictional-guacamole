package sec;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class ThoughtService {
    
    private List<Thought> thoughts;
    
    public ThoughtService() {
        this.thoughts = new ArrayList<>();
        this.thoughts.add(new Thought("Something"));
        this.thoughts.add(new Thought("Even more"));
        this.thoughts.add(new Thought("Yet more"));
    }
    
    public List<Thought> recvThoughts() {
        return this.thoughts;
    }
    
    public void addThought(Thought t) {
        this.thoughts.add(t);
    }
        
    public void deleteThought(int id) {
        this.thoughts.remove(id - 1);
    }
    
}
