package sec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GruntController {
    
    @Autowired
    private GruntService gruntService;
    
    @RequestMapping(value="/gruntsHub", method = RequestMethod.GET)
    public String recvGrunts(Model model) {
        
        model.addAttribute("grunts", gruntService.recvGrunts());
        
        return "gruntsHub";
    }
    
    @RequestMapping(value="/gruntsHub", method = RequestMethod.POST)
    public String newGrunt(Model model, @RequestParam Integer id, @RequestParam String message) {
        gruntService.newGrunt(new Grunt(id, message));
        
        return "redirect:/gruntsHub";
    }
    
}
