package sec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FictionalGuacamole {

    public static void main(String[] args) throws Throwable {
        SpringApplication.run(FictionalGuacamole.class);
    }
}