package sec;

public class Thought {
    
    private String text;
    
    public Thought(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
    
}
