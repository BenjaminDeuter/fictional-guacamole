# fictional-guacamole
Intentionally vulnerable web app written in Java

This intentionally vulnerable web application was built as an exercise during the [Cyber Security Base with F-Secure](https://cybersecuritybase.github.io/) course, a [MOOC](https://en.wikipedia.org/wiki/Massive_open_online_course) offered by the [University of Helsinki](https://www.helsinki.fi/en) in collaboration with [F-Secure](https://www.f-secure.com/).

The exercise consisted of creating an intentionally vulnerable web application which incorporates at least five different flaws from the [OWASP top ten list](https://www.owasp.org/index.php/Top_10_2013-Top_10). This is followed by every participant performing a penetration test on the web application of another participant.

In summary, to be able to find bugs, one needs to have a solid understanding how web applications work. Looking at it from the perspective of the developer is therefore a worthwhile exercise.

This example is based on the work of [ahm3dhany](https://github.com/ahm3dhany/Broken-Web-Application).

The vulnerabilities of this project are described in [Vulnerabilities.md](Vulnerabilities.md).
