# Vulnerabilities

## A1-Injection

> Injection flaws, such as SQL, OS, and LDAP injection occur when untrusted data is sent to an interpreter as part of a command or query. The attacker’s hostile data can trick the interpreter into executing unintended commands or accessing data without proper authorization.

The page `/gruntHub` is vulnerable to SQL injection as the query is generated dynamically and the SQL parser has no possibility to detect logic-altering injections inside the string it was given.

```
Connection c = DriverManager.getConnection(db, "sa", "");
String db_query = "INSERT INTO Grunts (id, message) VALUES ('" + g.getId().toString() + "', '" + g.getMessage() + "');";
Statement s = c.createStatement();
s.execute(db_query);
s.close();
c.close();
```

Most of the time, this exploitation deals with SELECT queries. In this case, we deal with an INSERT query, but can still perform an injection by breaking out of the single quotes and starting a new statement with a semicolon.

```
'); DROP TABLE Grunts; -- f
```

Note that we cannot inject the id parameter here, since this parameter is represented as an Integer in the code and only later cast to a String when the query is being built.

Therefore, we choose an arbitrary id and submit the above payload in the grunt field.

![](img/csrf01.png)

We immediately see that no grunts are displayed anymore since they were just deleted. This can also be seen in the Java error console which tells us that the code encountered an `org.h2.jdbc.JdbcSQLException`.

![](img/csrf02.png)

After the POST request, the browser was redirected and issued another GET request that triggered the SELECT statement.

```
ResultSet rs = c.createStatement().executeQuery("SELECT id, message FROM Grunts");
```

As the table did not longer exist, the exception was raised.

### Fix

Always using prepared statements is the recommended way to prevent SQL injections which can result in great damage to production systems.

```
String query = "INSERT INTO Grunts (id, message) VALUES (?, ?)";
PreparedStatement pstmt = connection.prepareStatement(query);
pstmt.setInt(1, g.getId());
pstmt.setString(2, g.getMessage());
pstmt.execute();
pstmt.close();
```

Here, the sql parser has appropriate knowledge of the values it expects. The logic can not be altered anymore.


## A3-Cross-Site Scripting (XSS)

> XSS flaws occur whenever an application takes untrusted data and sends it to a web browser without proper validation or escaping. XSS allows attackers to execute scripts in the victim’s browser which can hijack user sessions, deface web sites, or redirect the user to malicious sites.

After trying several input forms, we find that the input field in the thought panel is vulnerable to stored XSS.

![](img/xss01.png)

We see that our test vector gets injected into the site verbatim.

![](img/xss02.png)

This can be seen in the source code view within the browser.

![](img/xss03.png)

Or alternatively using Burp Suite (here we inject a script tag).

![](img/xss04.png)

Injecting `<script>alert(1)</script>` results in an alert window that is displayed by the webbrowser. This is a common proof of concept for Cross-site scripting (XSS) and demonstrates that we can execute arbitrary Javascript code in the victims browser session.

![](img/xss05.png)

This particular incident of XSS is stored as it gets written to the database, meaning it works even after logging in and out. As the admin user and the test user share a common database, the code gets executed as the admin user as well.

This may serve as a basis for further exploitation by [BeEF](http://beefproject.com/).

### Fix

This bug was caused by disabling escaping using `th:utext` in Thymeleaf.

```
<tr th:each="t: ${thoughts}">
    <!-- Text gets not escaped! Cross-site scripting possible! -->
    <td th:utext="${t.text}"></td>
</tr>
```

To remedy this bug, `th:text` should be used instead.

## A7-Missing Function Level Access Control
> Most web applications verify function level access rights before making that functionality visible in the UI. However, applications need to perform the same access control checks on the server when each function is accessed. If requests are not verified, attackers will be able to forge requests in order to access functionality without proper authorization.

The application is missing role based access control: The site `/admin` can be accessed with an ordinary user account.

This path be either be guessed or found with a dedicated tool like dirb. However, dirb does not find much, when we just let it run because it is not logged in.

```
$ dirb "http://localhost:8080/" | grep 200
+ http://localhost:8080/login (CODE:200|SIZE:452)
$
```

Therefore, we have to log in using the browser and then extract the cookie.

![](img/rbac01.png)

Now dirb can use this cookie and is able to find more paths.

```
$ dirb "http://localhost:8080/" -c "JSESSIONID=95F33680A5BF695B86735A642A2498CF" | grep 200
+ http://localhost:8080/admin (CODE:200|SIZE:3373)
+ http://localhost:8080/admin.cgi (CODE:200|SIZE:3373)
+ http://localhost:8080/admin.php (CODE:200|SIZE:3373)
+ http://localhost:8080/admin.pl (CODE:200|SIZE:3373)
+ http://localhost:8080/favicon.ico (CODE:200|SIZE:946)
+ http://localhost:8080/login (CODE:200|SIZE:452)
$
```

We can obviously see some false positives here, like `/admin.pl`. Apparently, Spring allows arbitrary suffixes for mapped pages. `/admin.foobar` therefore also return the same page as `/admin`.

### Fix

Inside the file [SecurityConfiguration.java](src/main/java/sec/SecurityConfiguration.java), we change these lines

```
h.authorizeRequests()
 .anyRequest()
 .authenticated();
```

to these

```
h.authorizeRequests()
 .antMatchers("/admin/**")
 .hasRole("ADMIN")
 .anyRequest()
 .authenticated();
```

This makes sure that only the admin user can access the `/admin` page.

## A8-Cross-Site Request Forgery (CSRF)

> A CSRF attack forces a logged-on victim’s browser to send a forged HTTP request, including the victim’s session cookie and any other automatically included authentication information, to a vulnerable web application. This allows the attacker to force the victim’s browser to generate requests the vulnerable application thinks are legitimate requests from the victim.

When we can trick the user into visiting out attacker-controlled website while being logged-in in the to-be-attacked web application, we can post arbitrary thoughts and grunts on the user's message board.

![](img/csrf01.png)

The two buttons perform a POST request with the test message "CSRF Demo". This is a proof of concept but can obviously be changed to anything.

```
<p>Under no circumstances should you click on this button:</p>
<form action="http://localhost:8080/gruntsHub" method="POST">
    <input type="hidden" value="1337" name="id">
    <input type="hidden" value="CSRF Demo" name="message">
    <input type="submit" value="Warning! Do not click this button!">
</form>

<p>Or this button!</p>
<form action="http://localhost:8080/myThoughts" method="POST">
    <input type="hidden" value="CSRF Demo" name="text">
    <input type="submit
</form>
```

When the user clicks, he gets redirected back to the web application where he can see that a new entry (thought or grunt) has been created without his consent.

![](img/csrf02.png)

This proof of concept can be tested using [csrfdemo.html](src/main/resources/csrfdemo/csrfdemo.html).

### Fix

Within the file [SecurityConfiguration.java](src/main/java/sec/SecurityConfiguration.java), the CSRF tokens feature has been intentionally disabled using `h.csrf().disable();`. Removing this statement will fix the vulnerability by adding unpredictable tokens to every form.

## A10-Unvalidated Redirects and Forwards

Mathias Bynens has written about this ([Link](https://mathiasbynens.github.io/rel-noopener/)) and there is a demo by Michał Zalewski ([Link](http://lcamtuf.coredump.cx/switch/)).

Basically, every site you link to cannot read the data of the original site due to the Same-origin policy. However, it can still change the value of the window.opener.location variable of the original site.

By clicking on the `Course page` link, a new tab is opened that loads a page containing the following Javascript code:

```
if (window.opener != null) {
    window.opener.location.replace('https://cybersecuritybase.github.io');
}
```

This redirects the original tab to the website `https://cybersecuritybase.github.io`. If this site got hacked someday, an attacker could setup a fishing website that once again prompts to user to login. This way, the attacker could steal the password from the user.

### Fix

Avoid opening the new page in another tab, i.e. do not use `target="_blank"`.

Alternatively, as Mathias Bynens points out, you can use `rel="noreferrer noopenner"` which should work for all major browsers.

The link inside the file [header.html](src/main/resources/templates/fragments/header.html) would then be:

```
<a target="_blank" rel="noreferrer noopenner" th:href="@{/coursepage}">Course page</a>
```
